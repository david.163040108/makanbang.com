import TheRestaurantDbSource from '../../data/therestaurantdb-source';
import { createLoadingTemplate, createRestaurantItemTemplate } from '../templates/template-creator';

const Home = {
  async render() {
    const homePage = `
      ${document.querySelector('#mainContent').innerHTML = createLoadingTemplate.show()}
      <div class="content">
        <h2 class="content__heading">Restaurant List</h2>
        <div id="restaurants" class="restaurants">
 
        </div>
        <span id="item-empty"></span>
      </div>
    `;
    return homePage;
  },

  async afterRender() {
    // Fungsi ini akan di panggil setalah render()
    const home = await TheRestaurantDbSource.List();
    const homeContainer = document.querySelector('#restaurants');
    const emptyRestaurantList = document.querySelector('#item-empty');

    if (home.length === 0) {
      emptyRestaurantList.innerHTML =
      `
      <h4>No movies to show</h4>
      `;
    }

    home.forEach((data) => {
      homeContainer.innerHTML += createRestaurantItemTemplate(data);
    });
    createLoadingTemplate.remove();
  },
};

export default Home;
