import FavoriteRestaurantIdb from '../../data/favorite-restaurant-idb';
import { createLoadingTemplate, createRestaurantItemTemplate } from '../templates/template-creator';

const Like = {
  async render() {
    const likePage = `
    ${document.querySelector('#mainContent').innerHTML = createLoadingTemplate.show()}
      <div class="content">
        <h2 class="content__heading">Your Favorite Restaurant</h2>
        <div id="restaurants" class="restaurants">
 
        </div>
        <span id="item-empty"></span>
      </div>
    `;
    return likePage;
  },

  async afterRender() {
    const restaurant = await FavoriteRestaurantIdb.getAllRestaurant();
    const restaurantContainer = document.querySelector('#restaurants');
    const emptyRestaurantList = document.querySelector('#item-empty');

    if (restaurant.length === 0) {
      emptyRestaurantList.innerHTML =
      `
      <h4>No movies to show</h4>
      `;
    }

    restaurant.forEach((data) => {
      restaurantContainer.innerHTML += createRestaurantItemTemplate(data);
    });
    createLoadingTemplate.remove();
  },
};

export default Like;
