import UrlParser from '../../routes/url-parser';
import TheRestaurantDbSource from '../../data/therestaurantdb-source';
import { createLoadingTemplate, createRestaurantDetailTemplate } from '../templates/template-creator';
import LikeButtonPresenter from '../../utils/like-button-presenter';
import PostReview from '../../utils/post-reviews';
import FavoriteRestaurantIdb from '../../data/favorite-restaurant-idb';

const Detail = {
  async render() {
    const detailPage = `
    ${document.querySelector('#mainContent').innerHTML = createLoadingTemplate.show()}
      <div id="restaurant" class="restaurant"></div>
      <div id="likeButtonContainer"></div>
    `;
    return detailPage;
  },

  async afterRender() {
    // Fungsi ini akan dipanggil setelah render()
    const url = UrlParser.parseActiveUrlWithoutCombiner();
    const items = await TheRestaurantDbSource.Details(url.id);
    const { restaurant } = items;
    const restaurantContainer = document.querySelector('#restaurant');
    restaurantContainer.innerHTML = createRestaurantDetailTemplate(restaurant);

    LikeButtonPresenter.init({
      likeButtonContainer: document.querySelector('#likeButtonContainer'),
      favoriteRestaurant: FavoriteRestaurantIdb,
      restaurant: {
        id: restaurant.id,
        name: restaurant.name,
        city: restaurant.city,
        rating: restaurant.rating,
        description: restaurant.description,
        pictureId: restaurant.pictureId,
      },
    });
    createLoadingTemplate.remove();
    const btnSubmit = document.querySelector('#btn-submit');
    const reviewId = document.querySelector('#review-id');
    const nameId = document.querySelector('#name-id');
    btnSubmit.addEventListener('click', async (e) => {
      e.preventDefault();
      if (nameId.value === '') {
        // eslint-disable-next-line no-alert
        alert('Not Empty');
      } else {
        await PostReview(url, nameId.value, reviewId.value);
        reviewId.value = '';
        nameId.value = '';
      }
    });
  },
};

export default Detail;
