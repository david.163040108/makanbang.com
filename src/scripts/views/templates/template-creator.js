/* eslint-disable no-unused-vars */
import CONFIG from '../../globals/api-endpoint';

const createRestaurantDetailTemplate = (item) => `
  <h2 class="restaurant__title">${item.name}</h2>
  <img class="restaurant__poster" src="${CONFIG.IMG.S + item.pictureId}" alt="${item.name}" />
  <div class="restaurant__info">
    <h3>Information</h3>
      <h4>City</h4>
      <p>${item.city}</p>
      <h4>Address</h4>
      <p>${item.address}</p>
      <h4>Rating</h4>
      <p>${item.rating}</p>  
  </div>

  <div class="restaurant__description">
    <h3>Overview</h3>
    <p>${item.description}</p>
  </div>
  <div class="restaurant-category">
      <h2>Category</h2>
      <ul>
        ${item.categories.map((category) => `<li>${category.name}</li>`).join(' ')}
      </ul>
  </div>
  <div class="menu-foods">
      <h2>Foods Menu</h2>
      <ul>
        ${item.menus.foods.map((food) => `<li>${food.name}</li>`).join(' ')}
      </ul>  
  </div>
  <div class="menu-drinks">
    <h2>Drinks Menu</h2>

    <ol>
      ${item.menus.drinks.map((drink) => `<li>${drink.name}</li>`).join(' ')}
    </ol>  
  </div>
  <div class="review-id">
      <h2 class="review-title">Consumer Review</h2>
      ${item.customerReviews.map((review) => `
          <div class="review-container">
              <div class="review-photo-profile">
                  <img src="${CONFIG.REVIEW + review.name}" alt="consumer photo profile">
              </div>
              <div class="review-body">
                  <h3 class="review-consumer-name">${review.name}</h3>
                  <small class="review-date-post">${review.date}</small>
                  <p class="review-content">${review.review}</p>
              </div>
          </div>
          `).join('')}
  </div>
  <div class="add__review">
    <h2 class="review-title">Add Review</h2>
    <div class="form-review">
      <form>
        <label for="name-id">Name</label>
        <input type="text" id="name-id" name="name-id"  placeholder="Your name..">
        <label for="review-id">Review</label>
        <input type="text" id="review-id" name="review-id" placeholder="Reviews..">
        <button type="submit" id="btn-submit" class="btn-submit">submit</button>
      </form>
    </div>
  </div>
`;

const createRestaurantItemTemplate = (restaurant) => `
  <div class="restaurant-item">
    <div class="restaurant-item__header">
        <img class="lazyload restaurant-item__header__poster" alt="${restaurant.name}"
            data-src="${CONFIG.IMG.S + restaurant.pictureId}" >
        <div class="restaurant-item__header__rating">
            <p>⭐️<span class="restaurant-item__header__rating__score">${restaurant.rating}</span></p>
        </div>
    </div>
    <div class="restaurant-item__content">
        <p class="restaurant-item__city">${restaurant.city}</p>
        <h3 class="restaurant__title"><a href="${`/#/detail/${restaurant.id}`}">${restaurant.name}</a></h3>
        <p>${restaurant.description}</p>
    </div>
  </div>
  `;

const createLikeRestaurantButtonTemplate = () => `
  <button aria-label="like this restaurant" id="likeButton" class="like">
     <i class="fa fa-heart-o" aria-hidden="true"></i>
  </button>
`;

const createUnlikeRestaurantButtonTemplate = () => `
  <button aria-label="unlike this restaurant" id="likeButton" class="like">
    <i class="fa fa-heart" aria-hidden="true"></i>
  </button>
`;

const createLoadingTemplate = {
  show() {
    return `
      <div class="loader-css">
      <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
      </div>
    `;
  },

  remove() {
    document.querySelector('.loader-css').remove();
  },
};

export {
  createRestaurantItemTemplate,
  createRestaurantDetailTemplate,
  createLikeRestaurantButtonTemplate,
  createUnlikeRestaurantButtonTemplate,
  createLoadingTemplate,
};
