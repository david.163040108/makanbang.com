import API_ENDPOINT from '../globals/api-endpoint';
import CONFIG_API from '../globals/config-api';

class TheRestaurantDbSource {
  static async List() {
    const response = await fetch(API_ENDPOINT.LIST);
    const responseJson = await response.json();
    return responseJson.restaurants;
  }

  static async Details(id) {
    const response = await fetch(API_ENDPOINT.DETAIL(id));
    return response.json();
  }

  static async postReview(data) {
    const response = await fetch(API_ENDPOINT.NEW_REVIEW, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Auth-Token': CONFIG_API.KEY,
      },
      body: JSON.stringify(data),
    });
    return response.json();
  }
}

export default TheRestaurantDbSource;
