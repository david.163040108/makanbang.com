import TheRestaurantDbSource from '../data/therestaurantdb-source';
import CONFIG_API from '../globals/config-api';

const PostReview = async (url, name, review) => {
  const data = {
    id: url.id,
    name,
    review,
  };

  const reviewDetail = document.querySelector('.review-id');
  const date = new Date().toLocaleDateString('id-ID', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });

  const addReview = `
    <div class="review-container">
    <div class="review-photo-profile">

    </div>
    <div class="review-body">
        <h3 class="review-consumer-name">${name}</h3>
        <small class="review-date-post">${date}</small>
        <p class="review-content">${review}</p>
    </div>
    `;

  const responsePost = await TheRestaurantDbSource.postReview(data);
  console.log(responsePost.message);
  reviewDetail.innerHTML += addReview;
};

export default PostReview;
