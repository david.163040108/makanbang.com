import CONFIG_API from './config-api';

const API_ENDPOINT = {
  LIST: `${CONFIG_API.BASE_URL}/list/`,
  DETAIL: (id) => `${CONFIG_API.BASE_URL}/detail/${id}/`,
  IMG: {
    S: `${CONFIG_API.BASE_URL}/images/small/`,
    M: `${CONFIG_API.BASE_URL}/images/medium/`,
    L: `${CONFIG_API.BASE_URL}/images/large/`,
  },
  REVIEW: `${CONFIG_API.IMG_REVIEW_NAME}`,
  NEW_REVIEW: `${CONFIG_API.BASE_URL}/review/`,
};
export default API_ENDPOINT;
