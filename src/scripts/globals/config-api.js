const CONFIG_API = {
  BASE_URL: 'https://restaurant-api.dicoding.dev',
  CACHE_NAME: new Date().toISOString(),
  DATABASE_NAME: 'restaurant-database',
  DATABASE_VERSION: 1,
  OBJECT_STORE_NAME: 'restaurant',
  WEB_SOCKET_SERVER: 'wss://movies-feed.dicoding.dev',
  IMG_REVIEW_NAME: 'https://ui-avatars.com/api/?name=',
  KEY: '12345',
};

export default CONFIG_API;
