/* eslint-disable no-unused-vars */
const assert = require('assert');

Feature('Review Movies');

Before(({ I }) => {
  I.amOnPage('/');
});

Scenario('Review restaurant', async ({ I }) => {
  I.amOnPage('/');

  I.seeElement('.restaurant-item');

  I.click(locate('.restaurant__title a').first());

  I.seeElement('.add__review form');

  const reviewText = 'Testing E2E anonymous';
  I.fillField('name-id', 'anonymous');
  I.fillField('review-id', reviewText);

  I.click('#btn-submit');

  const lastReview = locate('.review-content').last();
  const lastReviewText = await I.grabTextFrom(lastReview);

  assert.strictEqual(reviewText, lastReviewText);
});
